<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroceriesController;
//Volgorde hier maakt uit, dus hoe specifieker (net als CSS etc? ) die heeft priority. Dus de { grocery} heeft priority als..check video 23
Route::redirect('/', '/groceries');
Route::get('/groceries', 'App\Http\Controllers\GroceriesController@index')->name('groceries.index');
Route::get('/groceries/create', 'App\Http\Controllers\GroceriesController@create')->name('groceries.create');
Route::post('/groceries', 'App\Http\Controllers\GroceriesController@store')->name('groceries.store');
Route::get('/groceries/{grocery}/edit', 'App\Http\Controllers\GroceriesController@edit')->name('groceries.edit'); 
Route::match(['put', 'patch'],'/groceries/{grocery}', 'App\Http\Controllers\GroceriesController@update')->name('groceries.update');
Route::delete('/groceries/{grocery}', 'App\Http\Controllers\GroceriesController@destroy')->name('groceries.destroy'); 


Route::get('/welcome', function () { //voor oefenen met layout en etc.
    return view('welcome');
});




