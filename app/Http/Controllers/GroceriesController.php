<?php
namespace App\Http\Controllers;
use App\Models\Grocery;
use Illuminate\Http\Request;

class GroceriesController extends Controller
{
    function index() {
      
        $groceries = Grocery::all(); 
        return view('groceries.index', ['groceries' => $groceries]);    
    }
        public function create() {
        return view('groceries.create');
    }

    public function store() {
      
        Grocery::create($this->validateGrocery());
        return redirect(route('groceries.index'));
    }

    public function edit(Grocery $grocery) {
            return view('groceries.edit', ['groceries' => $grocery]); 
    }       //de compact function, wijst het door naar de juiste grocery, deze manier doet hetzelfde: return view('groceries.edit', compact('grocery'));

    public function update(Grocery $grocery) {
        
        $grocery->update($this->validateGrocery());
        return redirect(route('groceries.index'));
    }

    public function destroy(Grocery $grocery) {
        $grocery->delete();
        return redirect(route('groceries.index'));     
    }

    public function validateGrocery() {
        return request()->validate([
            'name' => ['required', 'min:2'],
            'number' => ['required', 'digits_between:1,1000'],
            'price' => ['required'], 'digits' 
        ]);
    }
}
