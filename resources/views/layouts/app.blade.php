<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boodschappen App</title>
</head>
    <div id="menu">
    <ul>
        <li><a href="{{ route('groceries.index')}}">Overzicht</a></li>
        <li><a href="{{ route('groceries.create')}}">Voeg product toe</a></li>
    </ul>
    </div>
<body>
    @yield ('content')
</body>
</html>