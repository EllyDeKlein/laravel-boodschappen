@extends ('layouts/app')
@section ('content')
    <table>
    @php         
                    $headerTexts = ['Name', 'Prijs', 'Aantal', 'Totaal'];
                    $total = 0;
    @endphp
                    @for($i=0;$i<count($headerTexts);$i++)   
                        <th>{{$headerTexts[$i]}} </th>                  
                    @endfor             
                    @for($i=0;$i<count($groceries);$i++)    
    @php          
                    $result = $groceries[$i]->price * $groceries[$i]->number;
                    $total += $result;
    @endphp         
                        <tr>
                                <td>{{$groceries[$i]->name}}</td> 
                                <td>{{$groceries[$i]->price}}</td> 
                                <td>{{$groceries[$i]->number}}</td> 
                                <td><?=$result?> </td> 
                                <td><a href="{{ route('groceries.edit', $groceries[$i]) }}">  edit  </a></td>
                                <td>
                                <form method="POST" action="{{ route('groceries.destroy', $groceries[$i]) }}">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Verwijder">
                                </form>            
                                </td>   
                        </tr>                     
                        @endfor          
    </table> 
        <div id="total">
        <p>Totaal:</p>
        <?=$total?>
        </div>  
@endsection
    

