@extends ('layouts/app')
@section('content') 
<div id='cssform'>
    <h1>Nieuwe boodschap</h1>
        <form method ="POST" action="/groceries">
        @csrf 
        <div class="field">
            <label class="label" for="name">Name</label>
        <div class ="control">
             <input class="input" type="text" name="name" id="name"  required> <?php // was helemaal niet nodig? om te voorkomen dat je naam niet weer hoeft in te vullenvalue="{{old('name')}}" ?>
             @if ($errors->has('name'))
             <p>{{$errors->first('name')}}</p>
             @endif 
        </div>
        <div class="field">
            <label class="label" for="number">Number</label>
        <div class ="control">
             <input class="input" type="number" name="number" id="number" required>
             @if ($errors->has('number'))
             <p>{{$errors->first('number')}}</p>
             @endif 
        </div>
        <div class="field">
            <label class="label" for="price">Price</label>
        <div class ="control">
             <input class="input" type="number" name="price" id="price" required> <?php //required hier is browser side only ?>
             @if ($errors->has('price'))
             <p>{{$errors->first('price')}}</p>
             @endif 
        </div>
        </div>
        <button type="submit">Voeg toe</button>
    </form>

</div>
@endsection
