@extends ('layouts/app')

@section('content')
<div id='cssform'>

    <h1>Boodschap aanpassen</h1>
        <form method ="POST" action="/groceries/{{$groceries->id}}">  <?php //Browser only understands a POST request so de @method will make it a put...? ?>
        @csrf 
        @method('PUT')
        <div class="field">
            <label class="label" for="name">Name</label>
        <div class ="control">
             <input class="input" type="text" name="name" id="name" value="{{$groceries->name}}">
        </div>

        <div class="field">
            <label class="label" for="number">Number</label>
        <div class ="control">
             <input class="input" type="number" name="number" id="number" value="{{$groceries->number}}">
        </div>

        <div class="field">
            <label class="label" for="price">Price</label>
        <div class ="control">
             <input class="input" type="number" name="price" id="price" value="{{$groceries->price}}">
        </div>
        </div>

        <button type="submit">Bijwerken</button>
    </form>
</div>
@endsection